<?php
$nombre = $_POST['nombre'];
$apellido = $_POST['apellido'];
$mail = $_POST['email'];
$empresa = $_POST['comentario'];
$dirección =$_POST['direccion'];
$departamento =$_POST['departamento'];
$contactame =$_POST['gridCheck'];

$header = 'From: ' . $mail . " \r\n";
$header .= "X-Mailer: PHP/" . phpversion() . " \r\n";
$header .= "Mime-Version: 1.0 \r\n";
$header .= "Content-Type: text/plain";

$mensaje = "Este mensaje fue enviado por " . $nombre ." ".$apellido. ",\r\n";
$mensaje .= "Su e-mail es: " . $mail . " \r\n";
$mensaje .= "Su dirección es: ".$dirección."\r\n";
$mensaje .= "Vive en: ".$departamento."\r\n";
$mensaje .= "¿Quiere que lo contacten? ".$contactame."\r\n";
$mensaje .= "Mensaje: " . $_POST['mensaje'] . " \r\n";
$mensaje .= "Enviado el " . date('d/m/Y', time());

$para = 'bscanonr@correo.udistrital.edu.co';
$asunto = 'Mensaje de mi sitio web';

mail($para, $asunto, utf8_decode($mensaje), $header);

header("Location:agradecimiento.html");
?>